<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/objets_infos_extras.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'objets_infos_extras_description' => 'Ajouter des informations extras à vos objets',
	'objets_infos_extras_nom' => 'Infos extras pour objets',
	'objets_infos_extras_slogan' => 'Des informations extras à vos objets'
);
