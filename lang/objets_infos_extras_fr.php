<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/objets_infos_extras.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Paramétrages',
	'champ_chambres_objets' => 'Activer l’ajout d’informations extras sur les contenus :',

	// O
	'objets_infos_extras_titre' => 'Infos extras pour objets',

	// T
	'titre_page_configurer_objets_infos_extras' => 'Parametres infos extras pour objets'
);
